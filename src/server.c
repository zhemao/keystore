#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include "server.h"
#include "map.h"
#include <ctype.h>
#include <unistd.h>


int servsock=0, clisock=0;
map * store;
int port;
int bindaddr;
pthread_t gc_thread;
char running=1;

int main(int argc, char *argv[]){
	struct sockaddr_in servaddr, cliaddr;
	int buflen = 512, len, res, x;
	char buf[len];
	struct timeval tv;

	store = create_map();
	signal(SIGINT, interrupted);

	if(argc>1){
		port = atoi(argv[1]);
	}
	else{
		port = DEFAULT_PORT;
	}
	bindaddr = LOCALHOST;

	servsock = socket(AF_INET, SOCK_STREAM, 0);
	if(servsock<0){
		perror("socket()");
		exit(EXIT_FAILURE);
	}

	servaddr.sin_family = AF_INET;
	servaddr.sin_addr.s_addr = bindaddr;
	servaddr.sin_port = htons(port);

	res = bind(servsock, (struct sockaddr *)&servaddr, sizeof(servaddr));
	if(res<0){
		perror("bind()");
		exit(EXIT_FAILURE);
	}

	res = getsockname(servsock, (struct sockaddr *) &servaddr, &len);
	if(res<0){
		perror("getsockname()");
		exit(EXIT_FAILURE);
	}
	listen(servsock, 5);
	printf("listening on port %d\n", ntohs(servaddr.sin_port));

	//pthread_create(&gc_thread, NULL, gc_loop, NULL);

	while(running){
		memset(&cliaddr, 0, sizeof(cliaddr));
		clisock = accept(servsock, (struct sockaddr *) &cliaddr, &len);
		inet_ntop(AF_INET, (void*) &cliaddr.sin_addr, buf, buflen-1);
		printf("Accepted connection from %s\n", buf);
		memset(buf, 0, buflen);
		res = recv(clisock, buf, buflen-1, MSG_DONTWAIT);
		if(res==-1){
			close(clisock);
			continue;
		}
		len = strlen(buf);
		chomp(buf, len);
		vector* vec = split(buf, " ", len);
		if(vec->length==0){
			send(clisock, "bad\n", 4, MSG_DONTWAIT);
		}
		else if(strcmp((char*)vector_get(vec, 0), "get")==0){
			printf("getting\n");
			handle_get(clisock, vec);
		}
		else if(strcmp((char*)vector_get(vec, 0), "put")==0){
			printf("putting\n");
			handle_put(clisock, vec);
		}
		else if(strcmp((char*)vector_get(vec, 0), "rm")==0){
			printf("removing\n");
			handle_remove(clisock, vec);
		}
		else if(strcmp((char*)vector_get(vec, 0), "dbg")==0){
			printf("debugging\n");
			handle_debug(clisock, vec);
		}
		else if(strcmp((char*)vector_get(vec, 0), "ttl")==0){
			printf("setting expiry\n");
			handle_ttl(clisock, vec);
		}
		else{
			printf("unrecognized command %s\n", (char*)vector_get(vec,0));
			send(clisock, "bad\n", 4, MSG_DONTWAIT);
		}
		destroy_vector(vec);
		close(clisock);
	}
	close(servsock);
	destroy_map(store);
	return 0;
}

void gc_loop(void *tid){
	while(running){
		map_gc(store);
		sleep(GC_SLEEP);
	}
	pthread_exit(0);
}

void parse_configuration(const char* conffile){
	FILE * f = fopen(conffile, "r");
	char buf[256];
	int len;
	vector * vec;
	char * key;
	char * val;
	while(fgets(buf, 255, f)){
		chomp(buf, strlen(buf));
		len = strlen(buf);
		if(len==0||buf[0]=='#')
			continue;
		vec = split(buf, "\t", strlen(buf));
		key = vector_get(vec, 0);
		val = vector_get(vec, 1);
		if(strcmp(key, "port")==0){
			port = atoi(val);
		}
		else if(strcmp(key, "listen")==0){
			inet_pton(AF_INET, val, (void*)&bindaddr);
		}
		else{
			printf("Unrecognized key %s in configuration.\n", key);
		}
		destroy_vector(vec);
	}
	fclose(f);
}

int handle_get(int fd, vector* args){
	char *data, *key;
	int len;
	if(args->length!=2){
		write(fd, "bad\n", 4);
		return 0;
	}
	key = (char*)vector_get(args, 1);
	len = strlen(key);
	data = (char*)map_get(store, key, len);
	if(data==NULL){
		data = "null";
	} else if (strlen(data)==0){
		data = "empty";
	}
	len = strlen(data);
	write(fd,  data, len);
	write(fd, "\n", 1);
	return 1;
}

int handle_put(int fd, vector* args){
	char *data, *key, *val;
	char *buf;
	int buflen=0, bufsize=1024;
	int keylen, vallen, x;
	if(args->length<3){
		send(fd, "bad\n", 4, MSG_DONTWAIT);
		return 0;
	}
	buf = (char*)malloc(sizeof(char)*bufsize);
	buf[0] = 0;
	for(x=2;x<args->length;++x){
		val = vector_get(args, x);
		vallen = strlen(val);
		buflen+=vallen;
		if(buflen+1>bufsize){
			bufsize = buflen+1024;
			buf = realloc(buf, bufsize);
		}
		strncat(buf, val, vallen);
		if(x<args->length-1){
			buflen++;
			strncat(buf, " ", 1);
		}
	}
	key = (char*)vector_get(args, 1);
	keylen = strlen(key);
	key[keylen] = 0;
	map_put(store, key, keylen, buf, buflen);
	printf("Putting in %s as %s\n", buf, key);
	send(fd, "ok\n", 3, MSG_DONTWAIT);
	free(buf);
	return 1;
}

int handle_remove(int fd, vector* args){
	char* key;
	int len;
	if(args->length==1){
		send(fd, "bad\n", 4, MSG_DONTWAIT);
		return 0;
	}
	key = (char*)vector_get(args, 1);
	len = strlen(key);
	map_remove(store, key, len);
	send(fd, "ok\n", 3, MSG_DONTWAIT);
	return 1;
}

int handle_debug(int fd, vector* args){
	char *key, *val;
	int keylen, vallen;
	int bufsize=1024, buflen=0;
	char* buf = (char*)malloc(sizeof(char)*bufsize);
	strcpy(buf, "debugging");
	int x;
	for(x=0;x<store->size;++x){
		key = vector_get(store->vol_keys, x);
		val = map_get(store, key, strlen(key));
		if(key!=NULL&&val!=NULL){
			keylen = strlen(key);
			vallen = strlen(val);
			buflen+=keylen+vallen+3;
			if(buflen+1>bufsize){
				bufsize = buflen+1024;
				buf = (char*)realloc(buf, bufsize);
			}
			strcat(buf, key);
			strcat(buf, ": ");
			strcat(buf, val);
			strcat(buf, "\n");
		}
	}
	send(fd, buf, strlen(buf), MSG_DONTWAIT);
	free(buf);
	return 1;
}

int handle_ttl(int fd, vector* args){
	char * key;
	int secs, keylen;
	if(args->length<3){
		send(fd, "bad\n", 4, MSG_DONTWAIT);
		return 0;
	}
	key = (char*)vector_get(args, 1);
	secs = atoi((char*)vector_get(args, 2));
	keylen = strlen(key);
	map_ttl(store, key, keylen, secs);
	send(fd, "ok\n", 3, MSG_DONTWAIT);
	return 1;
}

void chomp(char* line, int len){
	int pos;
	char c;
	pos = len;
	do{
		pos--;
		c = line[pos];
	} while(isspace(c));
	line[pos+1] = 0;
}

char* substr(char* str, int start, int end){
	char* sub = (char*)malloc(sizeof(char)*(end-start+1));
	strncpy(sub, str+start, end-start);
	return sub;
}

vector* split(char* buf, char* split, int len){
	int x, start;
	char* tok;
	vector* vec = create_vector();
	for(tok=strtok(buf, split);tok;tok=strtok(NULL, split)){
		vector_add(vec, tok, strlen(tok)+1);
	}
	return vec;
}

void interrupted(int param){
	int x;
	char *key, *val;
	running = 0;
	printf("Shutting down.\n");
	if(servsock>-1) close(servsock);
	if(clisock>-1) close(clisock);
	if(store){
		printf("Keys in cache: %d\n", (int)store->size);
		destroy_map(store);
	}
	exit(0);
}

