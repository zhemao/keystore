#ifndef KEYSTORE_H
#define KEYSTORE_H

#define DEFAULT_PORT 3357
#define LOCALHOST 16777343
#define GC_SLEEP 1

#include "vector.h"

void chomp(char* line, int len);
void interrupted(int param);
vector* split(char* buf, char* split, int len);
char* substr(char* buf, int start, int end);
int handle_get(int fd, vector* args);
int handle_put(int fd, vector* args);
int handle_remove(int fd, vector* args);
int handle_debug(int fd, vector* args);
int handle_ttl(int fd, vector* args);
void parse_configuration(const char* conffile);
void gc_loop(void *tid);

#endif
