#include "map.h"
#include <string.h>

map* create_map(){
	map* m = (map*)malloc(sizeof(map));
	int i;
	m->size=0;
	m->buckets = (item**)malloc(sizeof(item*)*NUM_BUCKETS);
	for(i=0;i<NUM_BUCKETS;++i)
		m->buckets[i] = NULL;
	m->keys = create_vector();
	m->vol_keys = create_vector();
	return m;
}

void* map_get(map* m, void* key, size_t n){
	if(key==NULL){
		return NULL;
	}
	size_t h = hash_func(key, n);
	item* itm = m->buckets[h];
	while(itm!=NULL){
		if(memcmp(key, itm->key, n)==0)
			return itm->val;
		itm = itm->next;
	}
	return NULL;
}

void map_put(map* m, void* key, size_t keylen, void* val, size_t vallen){
	size_t h = hash_func(key, keylen);
	item* itm = m->buckets[h];
	item* last = NULL;
	while(itm!=NULL){
		if(memcmp(key, itm->key, keylen)==0){
		if(itm->val!=NULL) free(itm->val);
		itm->val = malloc(vallen);
		memcpy(itm->val, val, vallen);
			return;
		}
		if(itm->next==NULL) last = itm;
		itm = itm->next;
	}
	itm = (item*)malloc(sizeof(item));
	itm->key = malloc(keylen);
	memcpy(itm->key, key, keylen);
	itm->val = malloc(vallen);
	memcpy(itm->val, val, vallen);
	itm->next = NULL;
	if(last==NULL) m->buckets[h] = itm;
	else last->next = itm;
	vector_add(m->keys, itm->key, keylen);
	m->size++;
}

void map_remove(map* m, void* key, size_t n){
	size_t h = hash_func(key, n);
	item *itm = m->buckets[h];
	item *last = NULL;
	int keyind;
	while(itm!=NULL){
		if(memcmp(key, itm->key, n)==0){
		if(last==NULL)
		m->buckets[h] = NULL;
		if(last!=NULL)
		last->next = itm->next;
		free(itm->key);
		free(itm->val);
		free(itm);
		keyind = vector_index(m->keys, key, n);
		vector_remove(m->keys, keyind);
		m->size--;
		return;
	}
	last = itm;
		itm = itm->next;
	}
}

void map_ttl(map* m, void* key, size_t n, int secs){
	if(key==NULL)
		return;
	time_t expiry = time(NULL)+secs;
	size_t h = hash_func(key, n);
	item* itm = m->buckets[h];
	while(itm!=NULL){
		if(memcmp(key, itm->key, n)==0){
			itm->expiry = expiry;
			vector_add(m->vol_keys, key, n);
			break;
		}
		itm = itm->next;
	}
}

time_t map_expiry(map* m, void* key, size_t n){
	if(key==NULL){
		return 0;
	}
	size_t h = hash_func(key, n);
	item* itm = m->buckets[h];
	while(itm!=NULL){
		if(memcmp(key, itm->key, n)==0)
			return itm->expiry;
		itm = itm->next;
	}
	return 0;
}

void map_gc(map* m){
	int x;
	char * key;
	int size = m->vol_keys->length;
	for(x=0;x<size;++x){
		key = (char*)vector_get(m->vol_keys, x);
		time_t expiry = map_expiry(m, key, strlen(key));
		if(expiry<time(NULL)){
			map_remove(m, key, strlen(key));
		}
	}
}

/* Uses FNV hash function, as given by Julien Walker's "The Art of Hashing" */
size_t hash_func(void* key, size_t n){
	unsigned char* str = (char*)key;
	size_t h = 2166136261;
	int i;
	for (i=0;i<n;++i)
		h = ( h * 16777619 ) ^ str[i];
	return h % NUM_BUCKETS;
}

void destroy_map(map* m){
	int x;
	item* itm;
	for(x=0;x<NUM_BUCKETS;++x){
	itm = m->buckets[x];
		while(itm!=NULL){
			free(itm->key);
			free(itm->val);
			free(itm);
			itm = itm->next;
		}
	}
	destroy_vector(m->keys);
	destroy_vector(m->vol_keys);
	free(m->buckets);
	free(m);
}
