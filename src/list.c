#include "list.h"
#include <stdlib.h>

linked_list* create_list(){
	linked_list* list = (linked_list*) malloc(sizeof(linked_list));
	list->length = 0;
	list->first = NULL;
	list->last = NULL;
	return list;
}

list_iter* list_iterator(linked_list* list, int init){
	list_iter* iter = (list_iter*)malloc(sizeof(list_iter));
	if(init==0){
		iter->current = list->first;
	}
	else if(init==1){
		iter->current = list->last;
	}
	else return NULL;
	iter->started = 0;
	return iter;
}

void list_add(linked_list* list, void* data){
	nodeptr node = (nodeptr)malloc(sizeof(struct linked_node));
	node->data = data;
	if(list->first==NULL){
		node->prev = NULL;
		node->next = NULL;
		list->first = node;
		list->last = node;
	}
	else{
		list->last->next = node;
		node->prev = list->last;
		node->next = NULL;
		list->last = node;
	}
	list->length++;
}

void* list_current(list_iter* iter){
	if(iter->started&&iter->current!=NULL)
		return iter->current->data;
	return NULL;
}

void* list_next(list_iter* iter){
	if(!iter->started&&iter->current!=NULL){
		iter->started=1;
		return iter->current->data;
	}
	if(iter->current!=NULL){
		iter->current = iter->current->next;
		return list_current(iter);
	}
	return NULL;
}

void* list_prev(list_iter* iter){
	if(!iter->started&&iter->current!=NULL){
		iter->started=1;
		return iter->current->data;
	}
	if(iter->current!=NULL){
		iter->current = iter->current->prev;
		return list_current(iter);
	}
	return NULL;
}

void* list_first(linked_list* list){
	return list->first->data;
}

void* list_last(linked_list* list){
	return list->last->data;
}

void* list_pop(linked_list* list){
	nodeptr last = list->last;
	list->last = last->prev;
	void* data = last->data;
	last->prev->next = NULL;
	free(last);
	return data;
}

void* destroy_list(linked_list* list){
	nodeptr cur = list->first;
	while(cur!=NULL){
		free(cur->prev);
		cur = cur->next;
	}
	free(list->last);
	free(list);
}

