#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void chomp(char* line, int len){
	int pos;
	char c;
	pos = len;
	do{
		pos--;
		c = line[pos];
	} while(isspace(c));
	line[pos+1] = 0;
}

int execute(struct sockaddr_in * servaddr, char * cmd, int len, char * res, int resmax){
	int status;
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if(fd<0) return -1;
	status = connect(fd, (struct sockaddr *)servaddr, sizeof(struct sockaddr_in));
	if(status<0) return -1;
	write(fd, cmd, len);
	memset((void*)res, 0, resmax);
	read(fd, res, resmax);
	chomp(res, strlen(res));
	close(fd);
	return 0;
}

int main(int argc, char *argv[])
{
	struct sockaddr_in server;
	struct hostent *hp;
	char buf[1024];
	char name[256];
	int port;
	int res;

	memset(name, 0, 256);
	server.sin_family = AF_INET;
	if(argc>1){
		strcpy(name, argv[1]);
	} else {
		strcpy(name, "localhost");
	}
	if(argc>2){
		server.sin_port = htons(atoi(argv[2]));
	} else {
		server.sin_port = htons(3357);
	}
	hp = gethostbyname(name);
	bcopy( hp->h_addr, &server.sin_addr, hp->h_length);

	while(1){
		memset(buf, 0, 1024);
		printf("keystore> ");
		fgets(buf, 1023, stdin);
		chomp(buf, strlen(buf));
		if(strcmp(buf, "quit")==0||strcmp(buf, "exit")==0){
			printf("bye now\n");
			break;
		}
		res = execute(&server, buf, strlen(buf), buf, 1023);
		if(res<0){
			perror("main()");
		} else {
			printf("%s\n", buf);
		}
	}
	return 0;
}
