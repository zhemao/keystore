#ifndef LIST_H
#define LIST_H

#define FIRST 0
#define LAST 1

struct linked_node{
	void* data;
	struct linked_node* next;
	struct linked_node* prev;
} linked_node;

typedef struct linked_node* nodeptr;

typedef struct{
	int length;
	nodeptr first;
	nodeptr last;
} linked_list;

typedef struct{
	nodeptr current;
	char started;
} list_iter;

linked_list* create_list();
list_iter* list_iterator(linked_list* list, int init);
void list_add(linked_list* list, void* data);
void* list_current(list_iter* list);
void* list_next(list_iter* list);
void* list_prev(list_iter* list);
void* list_first(linked_list* list);
void* list_last(linked_list* list);
void* list_pop(linked_list* list);

#endif
