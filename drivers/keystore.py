import socket
import pickle
import sys

EOL = '\r\n'
CHUNKSIZE = 1024

class KeystoreException(Exception):
    def __init__(self, msg):
        self.msg = msg
    def __str__(self):
        return 'KeystoreException: <'+self.msg+'>'

class Keystore:
    def __init__(self, host='localhost', port=3357):
        self.host = str(host)
        self.port = int(port)
    def sendcmd(self, cmd):
        sock = socket.socket()
        sock.connect((self.host, self.port))
        sock.send(cmd+EOL)
        data = sock.recv(CHUNKSIZE)
        retval = ""
        while data:
            retval+=data
            data = sock.recv(CHUNKSIZE)
        sock.close()
        return retval.rstrip()
    def get(self, key, unpickle=False):
        data = self.sendcmd('get '+key)
        if data=='null':
            return None
        if unpickle:
            return pickle.loads(data)
        return data
    def put(self, key, val, pickle=False):
        if pickle: 
            val = pickle.dumps(val)
        data = self.sendcmd('put '+key+' '+val)
        if data!='ok':
            raise KeystoreException('set command failed')
    def remove(self, key):
        data = self.sendcmd('rm '+key)
        data.rstrip()
        if data!='ok':
            raise KeystoreException('rm command failed')
            
