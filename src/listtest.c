#include "list.h"
#include <stdlib.h>
#include <stdio.h>

int main(int argc, char* argv){
	printf("%ld\n", sizeof(linked_list));
	printf("%ld\n", sizeof(struct linked_node));
	linked_list* list = create_list();
	linked_list* list2 = create_list();
	int x;
	char* buf = "hello";
	for(x=0;x<10;x++){
		list_add(list, (void*)buf);
		list_add(list2, (void*)buf);
	}
	
	list_iter* iter = list_iterator(list, 0);
	
	while(list_next(iter)!=NULL){
		printf("%s\n", (char*)list_current(iter));
	}

	destroy_list(list);
	
	list_pop(list2);

	free(iter);
	iter = list_iterator(list2, 1);
	
	while(list_prev(iter)!=NULL){
		printf("%s\n", (char*)list_current(iter));
	}
	destroy_list(list2);
	free(iter);
}
