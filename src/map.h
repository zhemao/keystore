#ifndef MAP_H
#define MAP_H

#include <stdlib.h>
#include "vector.h"
#include <time.h>

#define NUM_BUCKETS 3848921

struct item_t{
	void* key;
	void* val;
	time_t expiry;
	struct item_t * next;
};

typedef struct item_t item;

typedef struct{
	item** buckets;
	size_t size;
	vector* keys;
	vector* vol_keys;
} map;

map* create_map();
void map_put(map* m, void* key, size_t keylen, void* val, size_t vallen);
void* map_get(map* m, void* key, size_t n);
void map_remove(map* m, void* key, size_t n);
void map_ttl(map* m, void* key, size_t n, int secs);
time_t map_expiry(map* m, void* key, size_t n);
size_t hash_func(void* key, size_t n);
void destroy_map(map* m);

#endif
