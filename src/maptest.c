#include "map.h"
#include "vector.h"
#include <stdio.h>
#include <unistd.h>

int main(void){
	map* m = create_map();
	int h;
	char* res;
	map_put(m, (void*)"key", 3, (void*)"val", 4);
	map_ttl(m, (void*)"key", 3, 1);
	printf("%s\n", (char*)map_get(m, "key", 3));
	sleep(3);
	map_gc(m);
	printf("%s\n", (char*)map_get(m, "key", 3));
	destroy_map(m);
	return 0;
}
